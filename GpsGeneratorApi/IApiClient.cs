﻿using System;

namespace GpsGeneratorApi
{
    public enum SpeedUnits : int
    {
        /// <summary>
        /// Km per hour.
        /// </summary>
        KmPH = 0,
        /// <summary>
        /// Knots.
        /// </summary>
        Knots = 1,
        /// <summary>
        /// Miles per hour.
        /// </summary>
        MpH = 2
    }

    public interface IApiClient : IDisposable
    {
        /// <summary>
        /// Starts simulation.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops simulation.
        /// </summary>
        void Stop();

        /// <summary>
        /// Sets current or initial position.
        /// </summary>
        /// <param name="lat">Latitude, degrees. (+N -S)</param>
        /// <param name="lon">Longitude, degrees. (+E -W)</param>
        void SetPosition(double lat, double lon);
        
        /// <summary>
        /// Sets speed.
        /// </summary>
        void SetSpeed(double speed, SpeedUnits units);

        /// <summary>
        /// Changes cource to specified point.
        /// </summary>
        /// <param name="lat">Latitude, degrees. (+N -S)</param>
        /// <param name="lon">Longitude, degrees. (+E -W)</param>
        void MoveTo(double lat, double lon);

        /// <summary>
        /// Changes cource, degrees.
        /// </summary>
        /// <param name="cource"></param>
        void SetCource(double cource);

        /// <summary>
        /// Changes NMEA output settings. Changes will NOT be applied during simulation running.
        /// </summary>
        /// <param name="saveNmeaToFile">A value indicating whether to store NMEA log files.</param>
        /// <param name="folder">A folder where to store NMEA logs.</param>
        /// <param name="fileName">A file name without extension.</param>
        void SetNmeaLogSettings(bool saveNmeaToFile, string folder, string fileName);
    }
}
