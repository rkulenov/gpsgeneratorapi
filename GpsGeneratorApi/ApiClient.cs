﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace GpsGeneratorApi
{
    public sealed class ApiClient : IApiClient
    {
        private static CultureInfo enUs = new CultureInfo("en-US");

        void IDisposable.Dispose()
        {
        }

        void IApiClient.Start()
        {
            PipeInteropDispatcher.ProcessRequestAsync(new PipeMessage("Start")).Wait();
        }

        void IApiClient.Stop()
        {
            PipeInteropDispatcher.ProcessRequestAsync(new PipeMessage("Stop")).Wait();
        }

        void IApiClient.SetPosition(double lat, double lon)
        {
            var p = new Dictionary<string, string>()
            {
                {"Lat", lat.ToString(enUs)},
                {"Lon", lon.ToString(enUs)}
            };
            PipeInteropDispatcher.ProcessRequestAsync(new PipeMessage("SetPosition", p)).Wait();
        }

        void IApiClient.SetSpeed(double speed, SpeedUnits units)
        {
            var p = new Dictionary<string, string>()
            {
                {"Speed", speed.ToString(enUs)},
                {"Units", ((int) units).ToString()} // 0-kmph, 1-knots, 2-miles
            };
            PipeInteropDispatcher.ProcessRequestAsync(new PipeMessage("SetSpeed", p)).Wait();
        }

        void IApiClient.MoveTo(double lat, double lon)
        {
            var p = new Dictionary<string, string>()
            {
                {"Lat", lat.ToString(enUs)},
                {"Lon", lon.ToString(enUs)}
            };
            PipeInteropDispatcher.ProcessRequestAsync(new PipeMessage("MoveTo", p)).Wait();
        }

        void IApiClient.SetCource(double cource)
        {
            var p = new Dictionary<string, string>()
            {
                {"Cource", cource.ToString(enUs)},
            };
            PipeInteropDispatcher.ProcessRequestAsync(new PipeMessage("SetCource", p)).Wait();
        }


        void IApiClient.SetNmeaLogSettings(bool saveNmeaToFile, string folder, string fileName)
        {
            var p = new Dictionary<string, string>()
            {
                {"SaveNmeaToFile", (saveNmeaToFile ? 1 : 0).ToString(enUs)},
                {"Folder", folder},
                {"FileName", fileName}
            };
            PipeInteropDispatcher.ProcessRequestAsync(new PipeMessage("SetNmeaLogSettings", p)).Wait();
        }
    }
}