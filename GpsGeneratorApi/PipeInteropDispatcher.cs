using System;
using System.Threading.Tasks;

namespace GpsGeneratorApi
{
    internal sealed class PipeInteropDispatcher
    {
        private const string PIPE_NAME = @"gpsgenapi_4";

        private static async Task CreatePipeServer(string pipeName)
        {
            try
            {
                await new PipeClient(pipeName).WaitForPipe(10000);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(string.Format("Unable to instantiate Server: {0}", ex.Message), ex);
            }
        }

        public static async Task<PipeMessage> ProcessRequestAsync(PipeMessage request)
        {
            var needInstance = false;
            var result = default (PipeMessage);
            var pipe = new PipeClient(PIPE_NAME);

            try
            {
                result = await pipe.ProcessRequest(request);
            }
            catch (PipeServerBrokenPipeException)
            {
                needInstance = true;
            }
            catch (PipeServerNotFoundException)
            {
                needInstance = true;
            }

            if (needInstance)
            {
                await CreatePipeServer(PIPE_NAME);
                result = await pipe.ProcessRequest(request);
            }

            return result;
        }
    }
}