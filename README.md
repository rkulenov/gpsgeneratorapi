# GPS Generator API client library #

This is client library for [GPS Generator 4.2.x](http://avangardo.com) API.

Using this API you can:

* Start\stop simulation;
* Set speed (in knots, miles per hour, km per hour);
* Set course;
* Set destination point;
* Change current location;
* Change NMEA log settings (destination folder and file name);

### How to install? ###

Get sources and compile from [bitbucket repository](https://bitbucket.org/rkulenov/gpsgeneratorapi), or get it from [NuGet](https://www.nuget.org/packages/GpsGeneratorApi/):

```
#!bash#
Install-Package GpsGeneratorApi
```

### How to use? ###

* Run "GPS Generator";
* Compile and run test application;

### Sample code ###


```
#!c#

using (IApiClient c = new ApiClient())
{
  // Choose folder to store NMEA logs
  c.SetNmeaLogSettings(true, @"C:\temp\gpsgen\", "apitest");

  // Set initial position, cource and speed
  c.SetPosition(20.430108, -76.302665);
  c.SetSpeed(123, SpeedUnits.KmPH);
  c.SetCource(45);

  // Start simulation for 10 sec
  c.Start();
  Thread.Sleep(TimeSpan.FromSeconds(10));
  
  // Change destination point and speed
  c.MoveTo(22.24, -79.57);
  c.SetSpeed(10, SpeedUnits.Knots);

  // Continue smulation for 10 sec
  Thread.Sleep(TimeSpan.FromSeconds(10));

  c.Stop();
}

```