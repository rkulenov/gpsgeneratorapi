﻿using System;
using System.Threading;
using GpsGeneratorApi;

namespace ApiTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (IApiClient c = new ApiClient())
                {
                    c.SetNmeaLogSettings(true, @"C:\temp\gpsgen\", "apitest");

                    c.SetPosition(20.430108, -76.302665);
                    c.SetSpeed(123, SpeedUnits.KmPH);
                    c.SetCource(45);
                    c.Start();
                    Thread.Sleep(TimeSpan.FromSeconds(10));

                    c.MoveTo(22.24, -79.57);
                    c.SetSpeed(10, SpeedUnits.Knots);

                    Thread.Sleep(TimeSpan.FromSeconds(10));

                    c.Stop();
                }

            }
            catch (AggregateException e)
            {
                foreach (var ee in e.InnerExceptions)
                {
                    Console.WriteLine(ee.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("Press ENTER to exit");
            Console.ReadLine();
        }
    }
}
